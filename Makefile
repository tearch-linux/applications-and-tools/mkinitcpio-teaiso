DESTDIR=/
all: install

prepare:
	mkdir -p $(DESTDIR)/usr/lib/initcpio/hooks
	mkdir -p $(DESTDIR)/usr/lib/initcpio/install
	
install: prepare
	install hook_live_hook $(DESTDIR)/usr/lib/initcpio/hooks/live_hook
	install install_live_hook $(DESTDIR)/usr/lib/initcpio/install/live_hook
	
remove:
	rm -f $(DESTDIR)/usr/lib/initcpio/hooks/live_hook
	rm -f $(DESTDIR)/usr/lib/initcpio/install/live_hook
